package sample.connect.spring.atlaskit;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorldController {

    @GetMapping("/atlaskit")
    public String helloWorld() {
        return "atlaskit";
    }
}