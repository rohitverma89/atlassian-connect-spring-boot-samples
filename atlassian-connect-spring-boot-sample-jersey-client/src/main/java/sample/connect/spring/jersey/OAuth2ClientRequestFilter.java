package sample.connect.spring.jersey;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;

@Component
public class OAuth2ClientRequestFilter implements ClientRequestFilter {

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @Autowired
    private AtlassianHostRepository hostRepository;

    private String userKey;

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        AtlassianHostUser hostUser = new AtlassianHostUser(getHost(requestContext), Optional.of(userKey));
        String token = atlassianHostRestClients.authenticatedAs(hostUser).getAccessToken().getValue();
        requestContext.getHeaders().add("Authorization", String.format("Bearer %s", token));
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    private AtlassianHost getHost(ClientRequestContext requestContext) {
        URI requestUri = requestContext.getUri();
        String baseUrl = String.format("%s://%s", requestUri.getScheme(), requestUri.getHost());
        return hostRepository.findFirstByBaseUrl(baseUrl).get();
    }
}
