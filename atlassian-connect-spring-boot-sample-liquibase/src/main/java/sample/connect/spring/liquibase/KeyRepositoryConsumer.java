package sample.connect.spring.liquibase;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class KeyRepositoryConsumer {

    @Autowired
    private KeyRepository keyRepository;

    @EventListener
    public void asd(ContextRefreshedEvent contextRefreshedEvent) {
        LoggerFactory.getLogger(getClass()).info("Key count: {}", keyRepository.count());
    }
}
